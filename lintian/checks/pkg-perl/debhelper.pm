# pkg-perl/debhelper -- lintian check script for required debhelper versions -*- perl -*-
#
# Copyright © 2013 Niels Thykier <niels@thykier.net>
# Copyright © 2013, 2019 gregor herrmann <gregoa@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::pkg_perl::debhelper;

use strict;
use warnings;

use Lintian::Tags qw(tag);

sub run {
    my ( $pkg, $type, $info, $proc, $group ) = @_;

    # Only for pkg-perl packages
    return
        unless $info->field('maintainer')
        =~ /pkg-perl-maintainers\@lists\.alioth\.debian\.org/sm;

    # Only for non-cdbs packages
    return if $info->relation('build-depends-all')->implies('cdbs');

    # Module::Build::Tiny and debhelper version 9.20140227
    if ( $info->relation('build-depends-all')
        ->implies('libmodule-build-tiny-perl')
        && !$info->relation('build-depends')
        ->implies('debhelper (>= 9.20140227~)')
        && !$info->relation('build-depends')
        ->implies('debhelper-compat') )
    {
        tag 'module-build-tiny-needs-newer-debhelper';
    }
    return;
}

1;
