# pkg-perl/xs-abi -- lintian check script for XS target directory -*- perl -*-
#
# Copyright © 2014 Damyan Ivanov <dmn@debian.org>
# Copyright © 2014 Axel Beckert <abe@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::pkg_perl::xs_abi;

use strict;
use warnings;

use Lintian::Tags qw(tag);
use Lintian::Relation qw(:constants);

sub run {
    my ( $pkg, $type, $info, $proc, $group ) = @_;

    return if $info->field('architecture') eq 'all';

    my $depends = $info->relation('strong');
    my $api_ver = $depends->visit(
        sub {
            return $1 if /^perlapi-(\d[\d.]*)$/;
            return;
        },
        VISIT_OR_CLAUSE_FULL | VISIT_STOP_FIRST_MATCH
    );

    return unless $api_ver;

    require Dpkg::Version;
    $api_ver = Dpkg::Version->new($api_ver);

    return unless $api_ver >= '5.19.11';

    my $legacy_dir = $info->index('usr/lib/perl5/');
    return unless $legacy_dir;

    if ( $legacy_dir->children ) {
        tag( "legacy-vendorarch-directory", 'usr/lib/perl5' );
    }

    return;
}

1;
