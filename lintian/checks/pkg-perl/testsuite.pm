# pkg-perl/no-testsuite -- lintian check script for detecting a missing Testsuite header -*- perl -*-
#
# Copyright © 2013 Niels Thykier <niels@thykier.net>
# Copyright © 2013 gregor herrmann <gregoa@debian.org>
# Copyright © 2014 Niko Tyni <ntyni@debian.org>
# Copyright © 2018 Florian Schlichting <fsfs@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::pkg_perl::testsuite;

use strict;
use warnings;

use Lintian::Tags qw(tag);

sub run {
    my ( $pkg, $type, $info, $proc, $group ) = @_;

    # Only for pkg-perl packages
    return
        unless $info->field('maintainer')
        =~ /pkg-perl-maintainers\@lists\.alioth\.debian\.org/sm;

    # Only for source packages
    return unless $type eq 'source';

    if ( !defined $info->field('testsuite') )
    {
        tag 'missing-testsuite-header';
        return;
    }

    if ( $info->field('testsuite') ne 'autopkgtest-pkg-perl' )
    {
        tag 'nonteam-testsuite-header', $info->field('testsuite');
        return;
    }

    if ( !($info->index('META.json') && $info->index('META.json')->size)
        && !($info->index('META.yml') && $info->index('META.yml')->size)
        && !$info->index('debian/tests/pkg-perl/use-name')
    ) {
        tag 'autopkgtest-needs-use-name';
    }
    return;
}

1;
