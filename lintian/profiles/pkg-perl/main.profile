Profile: pkg-perl/main
Extends: debian/main
Enable-Tags-From-Check:
  pkg-perl/vcs,
  pkg-perl/debhelper,
  pkg-perl/cdbs,
  pkg-perl/xs-abi,
  pkg-perl/testsuite
