#!/bin/sh
set -e

cleanup() {
	cd $PDIR

	#Clean up after smoke-setup
	cleanup_file=debian/tests/pkg-perl/smoke-cleanup
	if [ -x $cleanup_file ]
	then
	    ( export TDIR; $cleanup_file )
	fi

	rm -rf $TDIR
}

TEMP=${AUTOPKGTEST_TMP:-${TMPDIR:-/tmp}}

TDIR=$(mktemp -d $TEMP/smokeXXXXXX)

PDIR=`pwd`

trap cleanup EXIT

file_list=debian/tests/pkg-perl/smoke-files
if [ ! -r $file_list ]; then
    # backward compatibility squared for now
    file_list=debian/tests/pkg-perl/test-files
fi
if [ ! -r $file_list ]; then
    # backward compatibility for now
    file_list=debian/tests/test-files
fi

export AUTOMATED_TESTING=1
export NONINTERACTIVE_TESTING=1

# overridable with smoke-env
PKG_PERL_PROVE_ARGS="--verbose --recurse --merge"
PKG_PERL_XVFB_ARGS="-a"

env_list=debian/tests/pkg-perl/smoke-env
if [ ! -r $env_list ]; then
    env_list=debian/tests/pkg-perl/env-smoke
fi
if [ -r $env_list ]; then
    eval $(sed '/^ *\(#\|$\)/d; s/^/export /' $env_list)
fi

if [ -r $file_list ]; then
    egrep -v '^ *(#|$)' $file_list | while read file; do
	[ -r $file ] && cp --parents -a $file $TDIR
    done
else
    [ -d t ] && cp -a t $TDIR
    [ -f test.pl ] && cp -a test.pl $TDIR
fi

skip_list=debian/tests/pkg-perl/smoke-skip
if [ ! -r $skip_list ]; then
    skip_list=debian/tests/pkg-perl/skip-smoke
fi
if [ -r $skip_list ]
then
    egrep -v '^ *(#|$)' $skip_list | while read file; do
        rm -f $TDIR/$file
    done
else
    # common nuisances, no value with runtime tests
    rm -f $TDIR/t/*pod.t
    rm -f $TDIR/t/*pod[-_]cov*.t # coverage
    rm -f $TDIR/t/*pod[-_]syn*.t # syntax

    # these should ideally be conditional to RELEASE_TESTING but normally aren't
    # example: libbest-perl_0.15-1
    rm -f $TDIR/t/boilerplate.t

    # we're not interested in perlcritic regressions
    # example: libhttp-body-perl
    rm -f $TDIR/t/*critic*.t

    # example: liborlite-statistics-perl
    rm -f $TDIR/t/97_meta.t

    # example: libfile-userconfig-perl
    rm -f $TDIR/t/99_pmv.t

    # we test compile all the modules separately
    rm -f $TDIR/t/*compile*.t
    # Dist::Zilla generates these sometimes
    # example: libmoosex-has-sugar-perl
    rm -f $TDIR/t/00-compile/*.t

    # various "author" tests not meant to be run with the build dependencies
    rm -f $TDIR/t/*author*.t
    rm -f $TDIR/t/*author/*.t
fi

# for Test::Pod
mkdir -p $TDIR/blib

# for 'use blib'
mkdir -p $TDIR/blib/lib $TDIR/blib/arch

# for misc tests that need something in lib or blib/lib/
# cf. libapache-authenhook-perl
mkdir -p $TDIR/lib/Debian/pkgperl
mkdir -p $TDIR/blib/lib/Debian/pkgperl
cat <<'EOF' > $TDIR/lib/Debian/pkgperl/Foobar.pm
package Debian::pkgperl::Foobar;
our $VERSION = '0.01';
1;
__END__
=head1 NAME

Debian::pkgperl::Foobar - dummy module for test checkers

=cut
EOF
cp $TDIR/lib/Debian/pkgperl/Foobar.pm $TDIR/blib/lib/Debian/pkgperl

if [ ! -e $TDIR/MANIFEST ]; then
    cat <<'EOF' > $TDIR/MANIFEST
lib/Debian/pkgperl/Foobar.pm
EOF
fi

if [ ! -e $TDIR/MANIFEST.SKIP ]; then
    cp /dev/null $TDIR/MANIFEST.SKIP
fi

# this is intended to be a last resort, please use it responsibly
setup_file=debian/tests/pkg-perl/smoke-setup
if [ -x $setup_file ]
then
    ( export TDIR; $setup_file )

    # Evaluate skip list a second time since smoke-setup might have
    # generated some of the to-be-skipped files.
    if [ -r $skip_list ]; then
        egrep -v '^ *(#|$)' $skip_list | while read file; do
            rm -f $TDIR/$file
        done
    fi
fi

tests_file=$(pwd)/debian/tests/pkg-perl/smoke-tests
cd $TDIR
if [ -r $tests_file ]; then
    test_targets=$(eval ls -d $(egrep -v '^#' $tests_file) 2>/dev/null || true)
fi

if command -v xvfb-run >/dev/null
then
    XVFB=xvfb-run
else
    XVFB=
    PKG_PERL_XVFB_ARGS=
    PKG_PERL_XVFB_SERVER_ARGS=
fi

if [ -z "$test_targets" ] && [ -d t ]; then test_targets=t; fi

if [ ! -n "$test_targets" ]; then
    echo 'Nothing to prove, skipping.'
else
    $XVFB $PKG_PERL_XVFB_ARGS ${PKG_PERL_XVFB_SERVER_ARGS:+-s} ${PKG_PERL_XVFB_SERVER_ARGS:+"$PKG_PERL_XVFB_SERVER_ARGS"} prove -I"$TDIR" --blib $PKG_PERL_PROVE_ARGS $test_targets 2>&1
fi

if [ -f test.pl ]; then $XVFB perl -I"$TDIR" ./test.pl; fi 2>&1

