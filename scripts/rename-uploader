#!/bin/sh

# Documentation, Copyright & Licence below

die() {
    echo $@ >&2
    exit 1
}

if [ $# -ne 3 ]; then
   die "Usage: $(basename $0) 'fullname' oldemail newemail"
fi

which sponge > /dev/null || die "Cannot find 'sponge'; apt-get install moreutils ?"

list_dirs(){
    find $@ -maxdepth 1 -mindepth 1 -type d | sed 's,.*/,,' | sort -u
}

sub(){
    perl -e 'my ($a, $b) = @ARGV; while(<STDIN>){ s/$a/$b/g; print; }' -- "$1" "$2"  < "$3" | sponge "$3"
}

for i in `list_dirs`; do
    cd $i
	echo "Processing $i"
	if [ -d .git ] && ! git diff --quiet ; then
	    echo "Dirty git repo ($i), skipping"
		cd ..
	    continue
    fi
    for i in debian/control debian/copyright; do
        sub "$2" "$3" $i
        sub "$2" "$3" $i
    done
    if [ -d .git ] && ! git diff --quiet ; then
        printf "\tUpdated email address\n"
        dch --release-heuristic=changelog --no-auto-nmu "Email change: $1 -> $3"
# Uncomment the next 4 lines to use git to commit/push rather than doing a mass commit/push with mr at the end
#		dpt salsa $i --off && \
#		git commit -a -m "Email change: $1 -> $3"
#		dpt salsa $i --on
#		git push
    fi
    cd ..
done

echo <<EOF
Done. When happy do:

 mr commit -m "Email change: $1 -> $3"
 dpt salsa kgb --all --off && \
 mr push
 dpt salsa kgb --all --on
EOF

exit 0

POD=<<'EOF'
=head1 NAME

dpt-rename-uploader - handle maintainer email changes

=head1 SYNOPSIS

B<dpt rename-uploader> I<full name> I<old email> I<new email>

To be run from F<git/packages> directory.

=head1 DESCRIPTION

B<dpt rename-uploader> walks over all package directories and changes any
occurrences of author's old email address in F<debian/control> and
F<debian/copyright> with their new email address. It is handy when an uploader
became a Debian Developer and wants to use their L<me@debian.org> address.

A suitable entry is added to F<debian/changelog>, but nothing is committed.

Repositories with uncommitted changes are not touched and a warning is issued.

=head1 COPYRIGHT & LICENSE

Copyright: 2009, Ryan Niebur <ryan@debian.org>

=over

=item 2010, 2012, 2014 Salvatore Bonaccorso L<carnil@debian.org>

=item 2011, gregor herrmann L<gregoa@debian.org>

=item 2012, Nathan Handler L<nhandler@ubuntu.com>

=back

License: Artistic or GPL-1+

=cut
EOF
