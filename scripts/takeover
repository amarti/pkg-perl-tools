#!/bin/sh

# Copyright: 2008, 2009, 2013, 2014 Damyan Ivanov <dmn@debian.org>
#            2008, 2011, 2018 gregor herrmann <gregoa@debian.org>
#            2008, 2009, Ryan Niebur <ryan@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.



set -e

MYDIR="$(readlink -f `dirname $0`)"

usage() {
    BN=`basename $0`
    cat <<EOF
Usage: $BN [options] package...

Options:
    -t path     Top location of local repository clones (containing packages/)
                Required. Defaults to the DPT_PACKAGES setting. Cf. dpt-config(5)
    -m URL      URL to a message in which the previous maintainer requests the
                takeover
    -a #        RFA/ITA bug number (conflicts with -m)
    -g URL      Existing Git repository to clone
    -n          Local operation only. Local repository is prepared, but
                nothing is changed on salsa.
    -h          show this text

<package> is the source package name that we take over of. You may list more
than one package.
EOF
    exit 0
}

if [ -n "$DPT_PACKAGES" ]; then
    TOP=${DPT_PACKAGES%/packages}
else
    TOP=""
fi
MAIL=''
RFA=''
GIT=''
LOCAL=''

set -u

while getopts t:m:a:g:hn o; do
    case $o in
        h) usage;;
        m) MAIL=$OPTARG;;
        t) TOP=$OPTARG;;
        a) RFA=$OPTARG;;
        g) GIT=$OPTARG;;
        n) LOCAL=1;;
    esac
done
shift `expr $OPTIND - 1`

if [ -n "$RFA" ] && [ -n "$MAIL" ]; then
    echo "-m and -a cannot be given at the same time"
    exit 1
fi

if [ -n "$GIT" ] && [ $# -gt 1 ]; then
    echo '-g option allows only one package name'
    exit 1;
fi

[ -n "$TOP" ] || usage

TMP=`mktemp -d`

trap "rm -rf $TMP" QUIT INT 0

inject_package()
{
    PACKAGE=$1
    cat <<EOF
***
*** Taking over $PACKAGE
***
EOF

    if [ -n "$GIT" ]; then
        cd $TOP/packages
        git clone $GIT $PACKAGE
        cd $PACKAGE
        for b in `git branch -r|grep -E -v '/HEAD |/master$'|sed -e 's,^  origin/,,'`; do
            git checkout -b $b origin/$b
        done
        git checkout master
        git remote remove origin
        cd ..
    else
        cd $TOP/packages
        if [ -x /usr/bin/gbp ]; then
            gbp import-dscs --debsnap --pristine-tar $PACKAGE
        else
            git-import-dscs --debsnap --pristine-tar $PACKAGE
        fi
    fi

    cd $PACKAGE

    if [ -z "$LOCAL" ]; then
        dpt salsa pushrepo
    fi

    TXT="Take over for the Debian Perl Group"
    if [ -n "$MAIL" ]; then
        TXT="$TXT on maintainer's request ($MAIL)"
    fi
    if [ -n "$RFA" ]; then
        TXT="$TXT; Closes: #$RFA -- RFA/ITA"
    fi

    dch --no-auto-nmu -i -c debian/changelog "$TXT"
    git add debian/changelog
    git commit -m '[takeover-for-pkg-perl] Document the takeover'

    PC_OPTS="-a -A -C -p \"$PACKAGE\" -c -H -V"
    if [ -n "$LOCAL" ]; then PC_OPTS="$PC_OPTS -n"; fi
    dpt packagecheck $PC_OPTS

    if [ -z "$LOCAL" ]; then
        dpt push
    fi
    rm -rf $TMP
}

[ -n "${1:-}" ] || usage

PACKAGES=''
CURPWD=$(pwd)
while [ -n "${1:-}" ]; do
    inject_package $1
    PACKAGES="$PACKAGES $1"
    shift
    cd $CURPWD
done

if [ -n "$LOCAL" ]; then
    echo "Nothing pushed to salsa"
    echo "You may want to inspect the resulting repositories for"
    echo $PACKAGES
    echo "and run dpt salsa pushrepo"
    echo "to create the salsa repository and push the local"
    echo  "repository there"
fi

exit 0

POD=<<EOF
=head1 NAME

dpt-takeover -- take over a package for the Debian Perl Group

=head1 SYNOPSIS

B<dpt takeover> -t I<top-directory> [I<option>...] I<package>...

=head1 DESCRIPTION

B<dpt takeover> helps taking over a package and adopting its control files for
maintenance by the Debian Perl group.

B<dpt takeover> first creates a local Git repository either from existing Git
repository specified with the B<-g> option, or by importing all package versions
available on snapshot.debian.org via L<gbp-import-dscs(1)>.

Next B<dpt takeover> adds suitable entries to F<debian/changelog>, using
information given by the B<-m> or the B<-a> options.

Next B<dpt takeover> runs C<dpt-packagecheck -a -A -C -c> to adjust the control
fields.

Finally, the resulting Git repository is pushed to the Debian collaborative
server, L<salsa.debian.org>, unless the B<-n> option is used.

=head1 OPTIONS

=over

=item B<-t> I<top-directory>  B<(mandatory)>

The location of the top level directory, containing F<packages/>.

=item B<-m> I<URL>

The address of the message with which the previous maintainer requests
adoption. It is included in the commit message and F<debian/changelog>.

Conflicts with B<-a>.

=item B<-a> I<number>

RFA/ITA bug number. Suitable C<Closes: #nnnnnn> is added to
F<debian/changelog>.

Conflicts with B<-m>.

=item B<-g> I<URL>

URL of an existing Git repository to clone. If not present L<gbp-import-dscs> is
used to prime the local Git repository from the package versions available on
snapshot.debian.org.

=item B<-n>

Local operation. The resulting Git repository is not pushed to
L<salsa.debian.org>.

=back

=head1 COPYRIGHT & LICENSE

Copyright 2008, 2009, 2013, 2014 Damyan Ivanov <dmn@debian.org>

Copyright 2008, 2011, 2018 gregor herrmann <gregoa@debian.org>

Copyright 2008, 2009, Ryan Niebur <ryan@debian.org>

This program is free software and can be distributed under the same terms as
Perl.

=cut
EOF
