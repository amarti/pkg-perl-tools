#!/usr/bin/python3

"""
=head1 NAME

lp-mass-subscribe - subscribe to many packages on Launchpad

=head1 SYNOPSIS

 $ get-ubuntu-packages | lp-mass-subscribe
 
=head1 DESCRIPTION

This script will connect to B<Ubuntu> launchpad and subscribe
pkg-perl-maintainers to the list of packages which it expects on the standard
input, one package per line. It is usually used together with
L<get-ubuntu-packages(1)>.

=head1 COPYRIGHT & LICENSE

Copyright 2010, Ansgar Burchardt L<ansgar@43-1.org>
Copyright 2012, Nathan Handler L<nhandler@ubuntu.com>
Copyright 2018, gregor herrmann L<gregoa@debian.org>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

=cut
"""

distribution_name = "ubuntu"
person_name       = "pkg-perl-maintainers"

from launchpadlib.launchpad import Launchpad
from sys import stdin

launchpad = Launchpad.login_with("lp-mass-subscribe", "production")

person = launchpad.people[person_name]
distribution = launchpad.distributions[distribution_name]

packages = map(lambda x: x.strip(), stdin.readlines())

for package_name in packages:
  package = distribution.getSourcePackage(name = package_name)
  if not package:
    print("{0:s}: does not exist (yet)".format(package_name))
    continue

  if package.getSubscription(person = person):
    print("{0:s}: {1:s} is already subscribed".format(package_name, person_name))
    continue

  package.addBugSubscription(subscriber = person)
  print("{0:s}: subscribed {1:s}".format(package_name, person_name))

# vim:set et sw=2:
