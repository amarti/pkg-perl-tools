#!/bin/sh

# Copyright: 2009, Ryan Niebur <ryan@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.

MESSAGE=""

while true; do
    case "$1" in
        --ignore|--ignore-version)
            MESSAGE="IGNORE-VERSION: $(dpkg-parsechangelog --show-field Version) # "
            shift
            ;;
        --unreleased)
            dch --dist UNRELEASED "" --force-distribution
            shift
            ;;
        --waits|--waits-for)
            shift
            FIRST_LINE="WAITS-FOR: $1"
            if [ -n "$2" ]; then
                FIRST_LINE="${FIRST_LINE} $2"
            fi
            shift
            shift
            ;;
        *)
            break
            ;;
    esac
done

F_MESSAGE=$(mktemp)
F_HEADER=$(mktemp)
F_REST=$(mktemp)

MESSAGE="${MESSAGE}$@"
echo > $F_MESSAGE
if [ -n "$FIRST_LINE" ]; then
    echo "$FIRST_LINE" | sed 's/^/  /' >> $F_MESSAGE
fi
if [ -n "$MESSAGE" ]; then
    echo "$MESSAGE" | fmt | sed 's/^/  /' >> $F_MESSAGE
fi
head -1 debian/changelog > $F_HEADER
sed '1 d' debian/changelog > $F_REST

cat $F_HEADER $F_MESSAGE $F_REST > debian/changelog
rm $F_HEADER $F_MESSAGE $F_REST

exit 0

POD=<<'EOF'

=head1 NAME

B<dpt-dch-note> -- add notes/TODO items to debian/changelog

=head1 SYNOPSIS

B<dpt dch-note> [I<option>...] [I<text>]

B<dpt dch-note> --unreleased 'TODO: fill debian/copyright'

B<dpt dch-note> --waits-for libtemplate-perl 1.03 needed for tests

=head1 DESCRIPTION

B<dpt dch-note> is a helper program for adding notes to F<debian/changelog>
file. These can be useful for keeping track of TODO items or signaling PET to
ignore new upstream releases.

=head1 OPTIONS

=over

=item B<--ignore>

=item B<--ignore-version>

Add an "IGNORE-VERSION: I<version>" comment. This is useful for instructing
PET that this upstream release contains no changes that warrant an upload, for
example changes in META files or fixes concerning only non-UNIX platforms.

=item B<--waits> I<package> I<version>

=item B<--waits-for> I<package> I<version>

Add a "WAITS-FOR: I<package> I<version>" comment. This is useful to indicate
that uploading of this package needs another package to be uploaded first. This
also instructs PET not to show this package as ready for uploading until the
required package version is available in the archive.

=item B<--unreleased>

Change distribution in F<debian/changelog> to C<unreleased>. This is done to
indicate that the package needs more work and is not ready for
reviewing/uploading.

=back

=head1 COPYRIGHT & LICENSE

Copyright: 2009, Ryan Niebur L<ryan@debian.org>

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut
EOF
