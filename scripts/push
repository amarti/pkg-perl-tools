#!/bin/sh

# Documentation, Copyright & Licence below

cd $(git rev-parse --git-dir)/..

die() {
    echo $@ >&2
    exit 1
}

tag_to_regexp() {
    sed -e 's/%(version)s/.*/g;s/^/^/;s/$/$/'
}

get_from_gbp() {
    variant=$1
    type=$2
    value="$(read_gbp_conf | grep -E "${variant}-${type} *= *" | awk -F'= *' '{print $2}' | tail -1)"
    if [ -z "${value}" ]; then
        if [ "${type}" = "branch" ]; then
            if [ "${variant}" = "debian" ]; then
                echo "master"
            else
                echo "${variant}"
            fi
        elif [ "${type}" = "tag" ]; then
            if [ "${variant}" = "debian" ] && [ "$(dpkg-source --print-format .)" = "3.0 (native)" ]; then
                echo ".*"
            else
                echo "${variant}/.*"
            fi
        else
            die "dpt push: get_from_gbp(): Unknown type ${type}"
        fi
    elif [ "${type}" = "branch" ]; then
        echo "${value}"
    elif [ "${type}" = "tag" ]; then
        echo "${value}" | tag_to_regexp
    else
        die "dpt push: get_from_gbp(): Unknown type ${type}"
    fi
}

read_gbp_conf() {
    for gbpconf in .gbp.conf debian/gbp.conf .git/gbp.conf; do
        if [ -e "${gbpconf}" ]; then
            cat "${gbpconf}"
        fi
    done | grep -Ev '^[[:space:]]*#'
}

REFS="$(get_from_gbp debian branch)"

for r in $(get_from_gbp upstream branch) pristine-tar refs/notes/commits; do
    if git rev-parse --verify --quiet $r > /dev/null; then
        REFS="$REFS $r"
    fi
done
git push origin $REFS \
    $(git tag|grep -E "$(get_from_gbp debian tag)|$(get_from_gbp upstream tag)") \
    "$@" \
    || exit $?

POD=<<'EOF'
=head1 NAME

dpt-push - push relevant packaging refs to origin Git remote

=head1 SYNOPSIS

B<dpt push> [ I<git argument...> ]

To be run from packaging working directory.

=head1 DESCRIPTION

B<dpt push> pushes the following refs to the C<origin> remote:

=over

=item C<master> branch (or whatever is set to debian-branch in gbp.conf)

=item C<upstream> branch (or whatever is set to upstream-branch in gbp.conf)

=item C<pristine-tar> branch

=item tags named C<debian/*> (or whatever is set to debian-tag in gbp.conf)

=item tags named C<upstream/*> (or whatever is set to upstream-tag in gbp.conf)

=item all tags, if the package's source format is C<3.0 (native)>

=back

=head1 COPYRIGHT & LICENSE

Copyright: 2013-2015, Damyan Ivanov E<lt>dmn@debian.orgE<gt>

Copyright: 2013-2015, Axel Beckert E<lt>abe@debian.orgE<gt>

Copyright: 2015-2018, gregor herrmann E<lt>gregoa@debian.orgE<gt>

License: Artistic or GPL-1+

=cut
EOF
