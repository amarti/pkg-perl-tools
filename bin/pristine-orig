#!/bin/sh
# documentation at the end
set -e
set -u

PKG=`dpkg-parsechangelog --show-field Source`
VER=`dpkg-parsechangelog --show-field Version | sed 's,^[0-9]\+:,,; s,-[^-]\+$,,'`

if [ -f /etc/devscripts.conf ]; then
    . /etc/devscripts.conf
fi

if [ -f ~/.devscripts ]; then
    . ~/.devscripts
fi

ORIG_DIR=${USCAN_DESTDIR:-..}

GOT_ONE=
for c in gz bz2 xz; do
    if pristine-tar checkout ${ORIG_DIR}/${PKG}_${VER}.orig.tar.$c 2>/dev/null; then
        GOT_ONE=$c
        break
    fi
done

if [ -z "$GOT_ONE" ]; then
    # repeat, with error reporting enabled
    for c in gz bz2 xz; do
        pristine-tar checkout ${ORIG_DIR}/${PKG}_${VER}.orig.tar.$c
    done
else
    echo ${ORIG_DIR}/${PKG}_${VER}.orig.tar.$GOT_ONE ready
fi

exit 0

POD=<<EOF
=head1 NAME

pristine-orig -- acquire upstream source of the current package version

=head1 SYNOPSIS

 pristine-orig

=head1 DESCRIPTION

B<pristine-orig> checks the current package name via F<debian/control>
and the current package version from F<debian/changelog> and calls
L<pristine-tar(1)> with the right file name so that you get a pristine
upstream source tarball ready.

It tries gzip, bzip2 and xz compression suffices.

The tarball is placed in the C<USCAN_DESTDIR> directory (configured via
L<devscripts.conf(5)>, see L<uscan(1)>), or in the parent directory if no
C<USCAN_DESTDIR> is configured.

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2011, 2017 Damyan Ivanov <dmn@debian.org>

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=cut
EOF
