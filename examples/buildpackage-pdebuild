#!/bin/bash

# Copyright 2010-2019, gregor herrmann <gregoa@debian.org>
# Copyright 2011-2012, Salvatore Bonaccorso <carnil@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the same terms as Perl itself.

# wrapper script for building and checking packages
# with [(svn|git)-buildpackage and] pdebuild

# Usage: 
# svn-bp: ~/.svn-buildpackage.conf:
#         svn-builder=buildpackage-pdebuild
# git-bp: ~/.gbp.conf:
#         builder = buildpackage-pdebuild
# or call it directly

BUILDER=
DEBBUILDOPTS=
DEB_BUILD_OPTIONS=
DEB_BUILD_PROFILES=
PDEBUILDOPTS=
PBUILDEROPTS=
VCS=
BUILDDIR=".."
ME="$(basename $0)"

# where are we?
[ -n "$SVN_BUILDPACKAGE" ] && VCS=svn
[ -n "$GBP_BUILD_DIR" ]    && VCS=git

# options: 
# D foo = distribution foo,
# b = binary-arch,
# a = binary-indep
# S = source,
# j = -jX,
# s = -sa,
# i = pdebuild-internal,
# w = twice,
# v = -vX,
# t = notest,
# n = noopt
# p = nostrip
# h = hardening,
# d = DH_VERBOSE
# H = hookdir
while getopts D:baSj:siwv:tnphdH: O; do
	case "$O" in
		# pbuilder example
		#P)
		#	BUILDER="--pbuilder pbuilder"
		#	PBUILDEROPTS="$PBUILDEROPTS --basetgz /var/cache/pbuilder/base.tgz"
		#	;;
		# qemubuilder example
		#Q)
		#	DEBBUILDOPTS="$DEBBUILDOPTS -aarmel"
		#	BUILDER="--pbuilder qemubuilder"
		#	PBUILDEROPTS="$PBUILDEROPTS --configfile /var/cache/pbuilder/armel/armel-rc"
		#	PBUILDEROPTS="$PBUILDEROPTS --basepath /var/cache/pbuilder/${OPTARG}-base.qemu"
		#	;;
		D)
			PBUILDEROPTS="$PBUILDEROPTS --basepath /var/cache/pbuilder/${OPTARG}-base.cow"
			;;
		b)
			# TODO pbuilder recommends its --binary-arch option
			# or not: #660756
			DEBBUILDOPTS="$DEBBUILDOPTS -B -m'${DEBFULLNAME} <${DEBEMAIL}>'"
			;;
		a)
			DEBBUILDOPTS="$DEBBUILDOPTS -A"
			;;
		S)
			DEBBUILDOPTS="$DEBBUILDOPTS -S"
			;;
		j)
			DEBBUILDOPTS="$DEBBUILDOPTS -j${OPTARG}"
			DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS parallel=${OPTARG}"
			;;
		s)
			DEBBUILDOPTS="$DEBBUILDOPTS -sa"
			;;
		i)
			PDEBUILDOPTS="$PDEBUILDOPTS --use-pdebuild-internal"
			;;
		w)
			PBUILDEROPTS="$PBUILDEROPTS --twice"
			;;
		v)
			DEBBUILDOPTS="$DEBBUILDOPTS -v${OPTARG}"
			;;
		t)
			DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nocheck"
			DEB_BUILD_PROFILES="$DEB_BUILD_PROFILES nocheck"
			;;
		n)
			DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS noopt"
			;;
		p)
			DEB_BUILD_OPTIONS="$DEB_BUILD_OPTIONS nostrip"
			;;
		h)
			export DEB_BUILD_HARDENING=1
			;;
		d)
			export DH_VERBOSE=1
			;;
		H)
			PBUILDEROPTS="$PBUILDEROPTS --hookdir ${OPTARG}"
			;;
		*)
			;;
	esac 
done
shift $((OPTIND - 1))


[ -n "$PBUILDEROPTS" ] && PBUILDEROPTS="-- $PBUILDEROPTS"
[ -n "$DEB_BUILD_OPTIONS" ] && export "DEB_BUILD_OPTIONS=${DEB_BUILD_OPTIONS:1}"
[ -n "$DEB_BUILD_PROFILES" ] && export "DEB_BUILD_PROFILES=${DEB_BUILD_PROFILES:1}"

pdebuild --buildresult $BUILDDIR/ $PDEBUILDOPTS --debbuildopts "$DEBBUILDOPTS" $BUILDER $PBUILDEROPTS "$@"

SUCCESS=$?

case $SUCCESS in
	0)
		URGENCY=normal
		;;
	*)
		URGENCY=critical
		;;
esac

if [ -x /usr/bin/notify-send -a -n "$DISPLAY" ] ; then
	notify-send -u $URGENCY -t 5000 "$ME finished" "$(dpkg-parsechangelog | egrep '^(Source|Version)')"
fi

if [ $SUCCESS = 0 -a -e $(dirname $0)/check-build ] ; then
	source $(dirname $0)/check-build
fi

# when called directly ...
if [ -d .pc ] && ! quilt applied >/dev/null 2>&1 ; then
	rm -rf .pc
fi

[ -n "$VCS" ] && \
	find $BUILDDIR -maxdepth 1 -type d -a -not -wholename "$BUILDDIR" -print0 | xargs -r0 rm -r

exit $SUCCESS
