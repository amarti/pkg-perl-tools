#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use File::Basename qw( dirname );

BEGIN {
    plan skip_all
        => "Set AUTHOR_TESTING and to perform these tests"
        unless $ENV{AUTHOR_TESTING};

    plan skip_all
        => "These tests require a working Debian::PkgPerl::Patch"
        unless eval { use Debian::PkgPerl::Patch; 1 };
}

my $patch = dirname(__FILE__) . '/dummy.txt.patch';
my $bug = new_ok( 'Debian::PkgPerl::Patch', [ patch => $patch ] );

my %info = $bug->retrieve_patch_info();
ok( exists $info{patch}, "retrieve patch name" );
is( $info{patch}, $patch, "verify patch name" );
ok( exists $info{Subject}, "retrieve patch info" );
like( $info{Subject}, qr/\w+/, "subject in patch is not empty" );
like( $info{From}, qr/\w+/, "from in patch is not empty" );

note("$info{patch} ($info{Subject}) by $info{From}");
done_testing();
