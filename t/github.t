#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

BEGIN {
    plan skip_all
        => "Set AUTHOR_TESTING to perform this test"
        unless $ENV{AUTHOR_TESTING};

    plan skip_all
        => "GitHub tests require DPT_GITHUB_OAUTH token,"
         . " a working Debian::PkgPerl::GitHub,"
         . " Debian::PkgPerl::Message,"
         . " Debian::PkgPerl::Patch,"
         . " and Test::MockModule"
        unless $ENV{DPT_GITHUB_OAUTH}
        and $ENV{DPT_GITHUB_OAUTH} =~ /^\w+$/
        and eval { use Debian::PkgPerl::Patch; 1 }
        and eval { use Debian::PkgPerl::Message; 1 }
        and eval { use Debian::PkgPerl::GitHub; 1 }
        and eval { use Test::MockModule; 1 };
}

use Test::RequiresInternet ( 'github.com' => 22 );

my %params = (
    url     => 'https://github.com/alexm/pkg-perl-dummy/issues',
    tracker => 'github',
    dist    => 'Foo-Bar',
    name    => 'My Name',
    email   => 'my.name@example.com',
    mailto  => 'your.name@example.com',
);

my %info = Debian::PkgPerl::Patch
    ->new( patch => 't/dummy.txt.patch' )
    ->retrieve_patch_info();

my $msg = Debian::PkgPerl::Message->new(
    %params,
    info => \%info,
    interactive => 0,
);

my $class = 'Debian::PkgPerl::GitHub';

subtest 'pull request' => sub {
    my $gh = new_ok( $class, [ message => $msg ] );

    my $url = $gh->forward();
    isnt( $url, undef, 'successful pull request' );
    diag $url;

    done_testing();
};

subtest 'fallback' => sub {
    my $mock = Test::MockModule->new($class);
    $mock->mock( forward_patch_as_pull_request => sub { die 'mock' } );

    my $gh = new_ok( $class, [
        message  => $msg,
        fallback => 1,
    ]);

    my $url = $gh->forward();
    isnt( $url, undef, 'failed pull request' );
    diag $url;

    done_testing();
};

subtest 'failing pull request' => sub {
    my %info = Debian::PkgPerl::Patch
        ->new( patch => 't/failing.patch' )
        ->retrieve_patch_info();

    my $msg = Debian::PkgPerl::Message->new(
        %params,
        info => \%info,
        interactive => 0,
    );

    my $gh = new_ok( $class, [ message => $msg ] );

    my $url = $gh->forward();
    isnt( $url, undef, 'failing pull request' );
    diag $url;

    done_testing();
};

done_testing();
