#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

BEGIN {
    plan skip_all
        => "Set AUTHOR_TESTING to perform these tests"
        unless $ENV{AUTHOR_TESTING};

    plan skip_all
        => "These tests require LWP::Simple"
         . " and a working Debian::PkgPerl::Bug"
        unless eval { use LWP::Simple; 1 }
        and eval { use Debian::PkgPerl::Bug; 1 };
}

use Test::RequiresInternet ( 'bugs.debian.org' => 80 );
use Test::RequiresInternet ( 'bugs.debian.org' => 443 );

my $bugs_html = get('https://bugs.debian.org/src:pkg-perl-tools');
my @bug_list = $bugs_html =~ /<a href="bugreport\.cgi\?bug=(\d+)">#\1<\/a>/g;

plan skip_all
    => "No open bugs were found in pkg-perl-tools"
    unless @bug_list;

note("Bug list: @bug_list");

my $bug_nr = $bug_list[0];
my $bug = new_ok( 'Debian::PkgPerl::Bug', [ bug => $bug_nr ]);

my %info = $bug->retrieve_bug_info();
ok( exists $info{bug}, "retrieve bug number" );
is( $info{bug}, $bug_nr, "verify bug number" );
ok( exists $info{Subject}, "retrieve bug $bug_nr info" );
like( $info{Subject}, qr/\w+/, "bug $bug_nr subject is not empty" );

note("bug $info{bug}: $info{Subject}");
done_testing();
