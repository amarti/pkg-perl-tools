use Test::More;
use Test::Pod;

my @pod_dirs = qw( bin scripts );

all_pod_files_ok( map( glob("$_/*"), @pod_dirs ) );
