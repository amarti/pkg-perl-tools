#!/usr/bin/perl

use strict;
use warnings;

use Test::More;
use Test::Differences;
use Test::Exception;
use File::Slurp 'read_file';
use DateTime;
use File::Copy 'copy';

BEGIN {
	plan skip_all => "Set AUTHOR_TESTING to perform these tests"
		unless $ENV{AUTHOR_TESTING};
	# FIXME
	plan skip_all => "This test hasn't been updated to changes in patchedit yet.";
}

use FindBin qw($Bin);
use lib "$Bin/lib";

my $script = $Bin.'/../../bin/patchedit';
unless ( my $return = do $script ) {
	warn "couldn't parse $script: $@" if $@;
	warn "couldn't do $script: $!" unless defined $return;
	warn "couldn't run $script" unless $return;
}

exit maint();

sub maint {

	my @tests = qw(
		no-meta
		no-meta2
		with-meta
		with-meta-and-extra-fields
		with-utf8-meta
		subject
		dpatch-no-meta
		dpatch-meta
	);
	plan tests => @tests * 2;

	local $ENV{'EDITOR'} = '/bin/true';
	foreach my $type (@tests) {
		foreach my $sub_type ('', '-o') {
			copy("t/patchedit/$type.orig", 't/patchedit/'.$type) or die "Copy failed: $!";
			local @ARGV = (($sub_type ? $sub_type : ()), '-f', 'edit', 't/patchedit/'.$type);
			main();
			my $fixed          = read_file('t/patchedit/'.$type);
			
			my $should_be_filename = 't/patchedit/'.$type.'.ok'.$sub_type;
			if (not -f $should_be_filename) {
				SKIP: {
					skip $should_be_filename.' file not found, skipping test', 1;
				};
				next;
			}
			
			my $should_be      = read_file($should_be_filename);
			eq_or_diff($fixed, $should_be, 'fixing '.$type.($sub_type ? ' '.$sub_type : ''));
			unlink('t/patchedit/'.$type);
		}
	}
	
	return 0;
}


do {
	no warnings 'redefine';
	sub DateTime::now {
		return DateTime->new(year => 2004, month => 2, day => 2);
	}
};
